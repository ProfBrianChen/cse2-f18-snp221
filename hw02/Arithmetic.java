//Samantha Pastoressa
//Semptember 9th, 2018
// Professor Chen, CSE002 - 311
//Homework 2
//Bio - You go shopping at a store and want to compute the cost of the items you bought, including the PA sales tax of 6%.

public class Arithmetic{
  
  public static void main(String args[]){

    int numPants = 3;
    //Number of pairs of pants
    double pantsPrice = 34.98;
      //cost per pair of pants
     
      //Number of sweatshirts
  int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
    
    
    double totalCostPants, totalCostShirts, totalCostBelts;
    totalCostPants = numPants * pantsPrice;
     totalCostShirts = numShirts * shirtPrice;
      totalCostBelts = numBelts * beltCost;
      
      double salesWithTaxBelts, salesWithTaxShirts, salesWithTaxPants;
        salesWithTaxBelts = (totalCostBelts * paSalesTax) + totalCostBelts;
      salesWithTaxShirts = (totalCostShirts * paSalesTax) + totalCostShirts;
      salesWithTaxPants = (totalCostPants * paSalesTax) + totalCostPants;
    
    
    double salesTaxBelts, salesTaxShirts, salesTaxPants;
      salesTaxBelts = totalCostBelts * paSalesTax;
      salesTaxShirts = totalCostShirts * paSalesTax;
      salesTaxPants = totalCostPants * paSalesTax;
    
 
      
      double totalCost, totalWithSalesTax, totalSalesTax, totalPaid;
    
        
    
      //total cost before tax
      totalCost = totalCostPants + totalCostShirts + totalCostBelts;
      totalWithSalesTax = (salesWithTaxPants + salesWithTaxShirts + salesWithTaxBelts) - totalCost;
      totalPaid = salesWithTaxPants + salesWithTaxShirts + salesWithTaxBelts;
    totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;
    
    double salesTaxShirts2, salesTaxShirts3, salesTaxShirts4;
    salesTaxShirts2 = salesTaxShirts * 100;
    salesTaxShirts3 = (int)salesTaxShirts2;
      salesTaxShirts4 = salesTaxShirts3 / 100;
    
   double salesTaxPants2, salesTaxPants3, salesTaxPants4;
    salesTaxPants2 = salesTaxPants * 100;
    salesTaxPants3 = (int)salesTaxPants2;
      salesTaxPants4 = salesTaxPants3 / 100;
    
    double salesTaxBelts2, salesTaxBelts3, salesTaxBelts4;
    salesTaxBelts2 = salesTaxBelts * 100;
    salesTaxBelts3 = (int)salesTaxBelts2;
      salesTaxBelts4 = salesTaxBelts3 / 100;
    
    double totalSalesTax4, totalSalesTax2, totalSalesTax3;
    totalSalesTax2 = totalSalesTax * 100;
    totalSalesTax3 = (int)totalSalesTax2;
    totalSalesTax4 = totalSalesTax3 / 100;
    
    double totalPaid2, totalPaid3, totalPaid4;
    totalPaid2 = totalPaid * 100;
    totalPaid3 = (int)totalPaid2;
    totalPaid4 = totalPaid3 / 100;
    

    
    System.out.println("The total cost of shirts is " + totalCostShirts + " and the total sales tax paid for this item is " + salesTaxShirts4);
          System.out.println("The total cost of pants is " + totalCostPants + " and the total sales tax paid for this item is " + salesTaxPants4);
        System.out.println("The total cost of belts is " + totalCostBelts + " and the total sales tax paid for this item is " + salesTaxBelts4);
    
    
     System.out.println("The total cost of the purchases before tax is " + totalCost);
     System.out.println("The total sales tax is " + totalSalesTax4);
     System.out.println("The total cost of the purchases with the sales tax is " + totalPaid4);
    
    
    
                          
  }
}
   