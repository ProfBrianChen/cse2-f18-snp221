// hw03 Convert
// Samantha Pastoressa
// CSE002 - 311
// September 13, 2018

import java.util.Scanner;
//imports scanner
public class Pyramid{
  public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in );
   //declares an instance of the Scanner object and calls the Scanner constructor
    System.out.print("The square side of the pyramid is (input length): ");
    // this asks the user to input a value for the length of the square side of the pyramid
    double squareSide = myScanner.nextDouble();
    // this declares squareSide a double and assigns the user's input as its value
System.out.print("The height of the pyramid is (input height): ");
    // this asks the user for the height of the pyramid
    double height = myScanner.nextDouble();
    // this declares height a double and assigns the user's input as its value
   double volume = squareSide * squareSide * height;
    // this declares volume a double, and uses an equation with previously declared variables to find it
    // in this case to find the volume, you square the squareSide and multiply it by the height
System.out.println("The volume inside the pyramid is: " + volume / 3); 
    // this returns the volume, given the numbers they chose, to the user
    


    
    
    
}   
  	}