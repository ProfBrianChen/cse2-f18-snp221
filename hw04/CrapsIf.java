// hw04 CrapsIf
// Samantha Pastoressa
// CSE002 - 311
// September 20, 2018


import java.util.Scanner;
//imports scanner
public class CrapsIf{
  public static void main(String[] args) {
  Scanner myScanner = new Scanner( System.in );
   //declares an instance of the Scanner object and calls the Scanner constructor
    System.out.print("If you would like to randomly cast dice input 1, if you like to state the two dice you would like to have evaluated input 2. ");
    int choice = myScanner.nextInt();
    int choice1 = 0;
    int choice2 = 0;
 
    // this asks the user to input 1 or 2 depending on whether they want a random number or want to put in their own values
    // it also declares variables and assigns them a value, but the value changes later through the code
    
    if (choice != 2 & choice != 1){
     System.out.print("Invalid Number ");
      System.exit(0);
    }
    else if (choice == 1){
     choice1 = (int)(Math.random()*6 + 1);
    choice2 = (int)(Math.random()*6 + 1);
    }
    else if (choice == 2){
     System.out.print("Input value of first die: ");
       choice1 = myScanner.nextInt();
      System.out.print("Input value of second die: ");
     choice2 = myScanner.nextInt();
    }
    // based off of what the user wanted to do - and what number they put in, they either get a random number generated for them or they input their own values
    // if the user did not input either 1 or 2, than this code informs them that the number they put in was invalid
    
    
   if (choice1 != 1 & choice1 != 2 & choice1 != 3 & choice1 != 4 & choice1 != 5 & choice1 != 6){
     System.out.println(" The number for the first die is invalid");
       System.exit(0);
   }
      else if (choice2 != 1 & choice2 != 2 & choice2 != 3 & choice2 != 4 & choice2 != 5 & choice2 != 6){
       System.out.println("The number for the second die is invalid");
          System.exit(0);
    }
    // this ensures that the user's inputs are valid numbers
      
   
    if (choice1 == 1 & choice2 == 1){
      System.out.println("Snake Eyes");
    }
    else if (choice1 == 2 & choice2 == 2){
      System.out.println("Hard four");
    }
    else if (choice1 == 3 & choice2 == 3){
      System.out.println("Hard six");
    }
    else if (choice1 == 4 & choice2 == 4){
      System.out.println("Hard Eight");
    }
    else if (choice1 == 3 & choice2 == 1){
      System.out.println("Hard Ten");
    }
    else if (choice1 == 5 & choice2 == 5){
      System.out.println("Boxcars");
    }
    // When the values of the dice are the same, there is a different name for them then when the sums are the sanme. 
    // I acknowledged this difference by first assigning names to the rolls in which the dice were the same
    // In the code below, I just assigned the names based off of what their sum is equal to
    else if (choice1 + choice2 == 3){
      System.out.println("Ace Deuce");
    }
    else if (choice1 + choice2 == 4){
      System.out.println("Easy Four");
    }
    else if (choice1 + choice2 == 5){
      System.out.println("Fever Five");
    }
    else if (choice1 + choice2 == 6){
      System.out.println("Easy Six");
    }
    else if (choice1 + choice2 == 7){
      System.out.println("Seven out");
    }
    else if (choice1 + choice2 == 8){
      System.out.println("Easy Eight");
    }
    else if (choice1 + choice2 == 9){
      System.out.println("Nine");
    }
    else if (choice1 + choice2 == 10){
      System.out.println("Easy Ten");
    }
    else if (choice1 + choice2 == 11){
      System.out.println("Yo-leven");
    }
    
   
  
    
    
  }
}
    
    
    
 