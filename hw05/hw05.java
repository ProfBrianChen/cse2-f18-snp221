import java.util.Scanner;
  public class hw05{
      public static void main (String [] args){
       Scanner myScanner = new Scanner( System.in );
        boolean hands;
        int RandomNumber = 0;
          int handsNumber = 0;
        String suit = "";
        String facecard = "";
        int case1 = 5;
        int card = 0;
        int value = 0;
        int cardlast = 0;
        int run = 0;
        int running = 0;
        int cardlast1, cardlast2, cardlast3, cardlast4, cardlast5;
        cardlast1 = cardlast2 = cardlast3 = cardlast4 = cardlast5 = 0;
        int RandomNumberLast = 0;
        int spades, clubs, hearts, diamonds; 
         clubs = hearts = diamonds = spades = 0;
        int fourkind, threekind, twokind, onekind;
        fourkind = threekind = twokind = onekind = 0;
       
      
        
        
        while(true){
            System.out.print("How many times would you like to generate hands? ");
            hands = myScanner.hasNextInt();
             if (hands){
                  handsNumber = myScanner.nextInt();
                break;
                }
                else{
                 System.out.println("Error - input of integer type needed: ");
                    myScanner.next();
     }    
    }
        // this will continue to ask the user for an integer until they provide one
   
         double divide = handsNumber;
        // because in the following part of the code, handsNumber runs until it = 0, there needed to be another variable that could be used to determine the probability
        
 
            
          while (handsNumber > 0){
            // this ensures that the code will continue to run while the handsNumber is greater than 0. 
            // The handsnumber is the number of hands that the user input previously, and this ensures that code produces the exact number of hands
                  while(case1 > 0){
                   // similarly for each hand we want 5 random integers. Because this code is inside the handsnumber while loop it will run this entirely before it runs handsnumber again
                    run = running;
                    cardlast = card;
                  

                        RandomNumberLast = RandomNumber;
                        RandomNumber = (int)(Math.random()*52 + 1);
                        while (RandomNumber == RandomNumberLast){
                          RandomNumber = (int)(Math.random()*52 + 1);
                            }
                        // System.out.println(RandomNumber);
                         if (RandomNumber <= 13){
                            facecard = "Diamonds";
                              }
                        else if(RandomNumber <= 26 & RandomNumber > 13 ){
                             facecard= "Clubs";
                                 }  
                        else if(RandomNumber <=39 & RandomNumber > 26 ){
                              facecard = "Hearts";
                                }
                         else if(RandomNumber <= 52 && RandomNumber > 39){
                               facecard = "Spades";
                                   }
                    
                   // this is more for just my own reference
                    
                     cardlast = RandomNumber % 13;
                    if (card == 0){
                    card = 14;
                    }
                    
                  // this coverts the random number to the number on the card, this allows the number for cards with different suits to be the same
                    // this will make it easier to compare the numbers and determine the type of hand the user was dealt

                    
                    
                     // the collection of code below enables each random number within the case1 while loop to be defined as variable, and therefore, compared to each other
                    // the run is the amount of times the case1 while loops is run, each time it produces a new variable, and we want to comapare each of these 5 new variables to each other to see which are the same
                    // this code also tests for all the types of hands. 
                    
                   if (run == 0){
                    cardlast1 = cardlast;
                     System.out.print(cardlast);
                    }
                    // this just takes the first number and assigns it a variable
                    else if(run== 1){
                        cardlast2 = cardlast;
                         if (cardlast2 == cardlast1){
                            value = value + 1;
                           System.out.print( value);
                          }
                      }
                    // this takes the second number and determines if it is equal to the first, and if it is, 1 is added to the value
                   else if (run == 2){
                      cardlast3 = cardlast;
                          if (cardlast3 == cardlast2){
                               value = value + 1;
                          }
                           if (cardlast3 == cardlast1){
                            value = value + 1;
                           }
                             System.out.print( value);
                    }
                    //similarly, this compares the third number to the first and second
                   else if(run == 3){
                       cardlast4 = cardlast;
                       if (cardlast4 == cardlast3){
                            value = value + 1;
                       }
                      if (cardlast4 == cardlast2){
                            value = value + 1;
                       }
                      if (cardlast4 == cardlast1){
                            value = value + 1;
                       }
                          System.out.print(value);
                     }
                    else if(run == 4){
                       cardlast5 = cardlast;
                      if((cardlast5 == cardlast4) && (value == 3)){
                        value = 3;
                      }
                      // when running this code I noticed hands with both 3 of a kind and a 2-pair were considered a 4 of a kind, so this part ensures that if this is the case, the hand will still be considered a three of a kind
                         else if (cardlast5 == cardlast4){
                             value = value + 1;
                        if (cardlast5 == cardlast3){
                            value = value + 1;
                            }
                        if (cardlast5 == cardlast2){
                            value = value + 1;
                            }
                        if (cardlast5 == cardlast1){
                            value = value + 1;
                            }
                         }
                            System.out.print(value);
                     }
                    
                   
                    
                    
                    
                       running = run + 1;
                    
 
                        
                   
                    if (suit == ""){
                       System.out.println("You picked the " + cardlast + " of " + facecard);
                        }
                        else {
                      System.out.println("You picked the " + suit + " of " + facecard);
                         }
                    // this is just for my reference
                    
                     case1 = case1 - 1;   
                  }    
            
            
                    switch(value){
                         case 4:
                        fourkind = fourkind + 1;
                        break;
                        case 3:
                       threekind = threekind + 1;
                        break;
                        case 1:
                        onekind = onekind + 1;
                        break;
                         case 2:
                        twokind = twokind + 1;
                        break;
                  }
            // the variable "value" above talled the number of pairs, and this code assigns the number to the type of hand it was
                   
         
              running = 0;
            value = 0;
             case1 = 5;
            //because we want the case1 while loop to continously run until the handsnumber is equal to 0, we had to ensure that it was reset to 5 each time the handsnumber while loop is run
          handsNumber = handsNumber - 1;      
        
           }
        
        

  
          System.out.println("Four of a kind: " + fourkind + " Three of a kind: " + threekind + " Two pairs " + twokind + " One pair: " + onekind  );
 
       System.out.println(" The number of loops: " + divide );
        System.out.println(" The probability of Four-of-a-kind: " + fourkind/divide);
       System.out.println(" The probability of Three-of-a-kind: " + threekind/divide);
        System.out.println(" The probability of Two-pair: " + twokind/divide);
        System.out.println(" The probability of one-pair: " + onekind/divide);

        

        
        
             
    
  }  
}
        