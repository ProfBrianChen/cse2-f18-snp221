import java.util.Scanner;
  public class EncryptedX{
      public static void main (String [] args){
       Scanner myScanner = new Scanner( System.in );
        
        boolean Number1;
        int integer = 0;
        int lastValue = 0;
       int number = 0;
        
        while(true){
            System.out.print("Input an integer between 1 and 100? ");
            Number1 = myScanner.hasNextInt();
             if (Number1){
                  integer = myScanner.nextInt();
                break;
                }
                else{
                 System.out.println("Error - input of integer type needed: ");
                    myScanner.next();
     }    
    }
        // this just ensures that the user's input is an integer
        
        
        
        // the only way that I could think of to create this pattern was to consider the sums of numbers
           // I wrote out the values of i and j and figured out which ones resulted in a " "
           //the blanks occured when the sum was equal to the integer + 1, and when the sum equaled 2 in the first row, 4 in the second, 6 in the third ect. 
        
         for(int i = 1; i <= integer; i++) {
           number = number + 1;
            // this code considers how many times the first for loop is run
           // this basically counts the number of rows, which is helpful for determing when the value (i+J) equals the sum that is a " "
         for(int j = 1; j <= integer; j++){
           int value = i + j;
           // this just  adds the values of i and j
          if(value == integer + 1 || number*2 == value){
          System.out.print(" ");
      //you want to consider when a sum of 2 is in the 1st row, a sum of 4 is in the 2nd row ect...
       //by multiplying the "number" by 2, then you will get the same value for the "number" and for the "value" (sum that results in a " "), so all you have to consider is when these equal each other            
       //it is important to this by row number and not just sums, bc a sum of 8 can, and will, occur in multiply rows, but you only want to consider it in the 4th row
      // this code also acknowledges that there is a " " when the sum is equal to the integer + 1
         }
           else {
          System.out.print("*");
        }
           
         }  
          System.out.println();
     }
             
   }
  }