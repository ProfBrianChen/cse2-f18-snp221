import java.util.Scanner;
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
 String[] hand = new String[5]; 
int numCards = 5; 
  // I set numCards equal to 5 because that's what the sample output had, I was confused on where we were supposed to get the value for the number of cards
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
//System.out.print(cards[i]+" "); 
  // I deleted this because it is repeated with the printArray method
} 
System.out.println();
printArray(cards); 
  System.out.println();
  System.out.println("Shuffled: ");
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
  System.out.println();
   hand = getHand(cards, index, numCards); 
   printArray(hand);
   index = index - numCards;
  System.out.println();
   System.out.print("Enter a 1 if you want another hand drawn "); 
   again = scan.nextInt(); 
}  
  } 
  
  public static String shuffle(String[] list){
    int i = 0;
    //System.out.println();
    for( i = 0; i < 51; i++){
      int random = (int)(list.length*Math.random());
      //the code below swaps the values of the random number in the list with the value at the index [0]
      String swap = list[random];
      list[random] = list[0];
      list[0] = swap;
     //System.out.print(list[0] + " ");
    }
   return list[0];
  } 
  
  
 
  public static String[] getHand(String [] list, int index, int numCards){
     String card1 = " ";
    String [] array = {card1};
    System.out.println("Hand: ");
    // in the code above, I set the number of cards to 5, because that is what the sample code had    
    for(int i = 0; i < numCards; i++){
      //this loop goes through the number of cards
      if(index < 0 && numCards != 0){
        index = 51;
        // it was important to reset index to 51, because if the number of cards is greater than the index, than the index would be 0
        shuffle(list);
        // this calls the shuffle method which reshuffles a new deck of cards. This will add cards from the new shuffled deck to the hand if the numCards is greater than the number of cards in the deck (which is represented by the index)
     }

      card1 = list[index];
      //this assigns the card at the first index to card1
      System.out.print(card1 + " ");
      index = index - 1;
      //just as you increment the number of cards by 1 each time in the loop, you also want to increment the index by negative one, so that you get the card at the susbsequent index each loop
    }
    String [] getHand = array;
    return getHand;
  } 
  
  
  
  
 public static void printArray(String[] list){
   for(int i = 0; i < list.length; i++){
    System.out.print(list[i] + " ");
     // this code just prints out the elements of the array list
   }
 }
  
  
  
  
}
