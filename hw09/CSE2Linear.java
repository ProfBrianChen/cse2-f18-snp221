// hw09 CSE2Linear
// Samantha Pastoressa
// CSE002 - 311
// November 23rd, 2018

import java.util.Scanner;
public class CSE2Linear{
  public static void main(String [] args){
    Scanner myScanner = new Scanner(System.in);
    int i = 0;
     int integerLast = 0;
    int [] array = new int[15];
  for(i = 0; i < 15; i++){
    // this loop allows the user to input 15 integers and than tests them for different criteria
      while(true){
        System.out.print("Input an integer");
        boolean check = myScanner.hasNext();
        if (check){
            int integer = myScanner.nextInt();
           if (integer < 0 || integer > 100){
             System.out.println("Error: int is out of bounds");
            }
           if (integer < integerLast){
             System.out.println("Error: int is out of bounds");
            }
            else{
            array[i] = integer;
              integerLast = integer;
              // this creates a variable for the last integer, which is used above to ensure that the user inputs numbers in ascending order
            break;
               }
          }
          else{
          System.out.println("Error - input of integer type needed: ");
          myScanner.nextInt();
                // THIS isnt working
              }
            }
         }
    // the code below calls methods and asks the users for inputs
    printArray(array);
    System.out.println();
    System.out.print("Enter a grade to be searched for: ");
    int grade = myScanner.nextInt();
    binarySearch(array, grade);
    Scramble(array);
    printArray(array);
    System.out.print("Enter a grade to be searched for: ");
    int grade2 = myScanner.nextInt();
    linearSearch(array, grade2);
    
    
    }
  
  public static void printArray (int [] array){
 for(int i = 0; i < 15; i++){
    System.out.print(array[i] + " ");
   // this just prints out the elements of the array
      }
 }
  
  public static void Scramble(int [] array){
    for(int i = 0; i < 15; i++){
     int random = (int)(array.length*Math.random());
      //the code below swaps the values of the random number in the list with the value at the index [0]
      int swap = array[random];
      array[random] = array[0];
      array[0] = swap;
    }
    }
  
  public static void linearSearch(int [] array, int grade2){
    int found = 0;
    for(int i = 0; i < 15; i++){
      if(grade2 == array[i]){
        // this is just a simple for loop that goes through each element in the array and determines whether or not it is equal to the grade
        System.out.println("Found");
        found = found + 1;
        System.exit(0);
        // once you are sure that the grade is found, you can exit the loop
      }
      
    }
    
    if(found == 0){
      System.out.println("Not Found");
      // to ensure that "not Found" didnt print on each element of the array that wasn't the grade, I created a variable called check that would equal one if the grade was found and continue to equal zero if it wasnt
      // it is outside of the loop so that it only prints once, if the grade is not in the array
    }
  }

     public static void binarySearch(int [] array, int grade){
    int low = 0;
       int found = 0;
      int high = array.length -1;
       int iterations = 0;
       while(high >= low){
         iterations = iterations + 1; 
         // each time the loop is run an iteration occurs
         int mid = (low + high)/2;
         // this finds the middle of the array which is important for binary search because it takes an integer in the middle of the array and than determines whether or not the target value is higher or lower and so on
         if (grade < array[mid]){
           high = mid - 1;
           //if the grade is less than the middle of the array (which is ordered) than you know that the number you are looking for must be in the first half of the array
           // by making the high the middle of the array - 1, and keeping the same low, you have made it so that the binary search will only continue in the first half
         }
         else if (grade == array[mid]){
           // when the number you are looking for is equal to the number in the array than you have found it
           System.out.println("Found");
           found = found + 1;
           // the reason I did this was because i wanted only one not found to print, instead of it printing each time the grade wasnt found
           System.out.println("Iterations: " + iterations);
           return;
         }
         else{
           low = mid + 1;
           // this is similar to the first if statement, however, this is what happens when the grade is greater than the mid
        // by making the low equal to the mid + 1 and keeping the high the same, this ensures that the search occurs in the second half of the array
         }
       }
       if(found == 0){
         // this is outside of the for loop because once the grade is found, the variable found will equal one
         // however if it isnt found than the integer is not in the array and the found variable would still equal zero
      System.out.println("Not Found");
         System.out.print("Iterations: " + iterations);
         System.out.println();
       }
       // the majority of this code I got from class notes
  }

  
  
  
  }
  
  
  
  
