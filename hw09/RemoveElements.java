// hw09 RemoveElements
// Samantha Pastoressa
// CSE002 - 311
// November 23rd, 2018

import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  public static int [] randomInput(){
      int [] list = new int[10];
    // this creates an array of length 10
    for(int i = 0; i < 10; i++){
     list[i]  = (int)(list.length*Math.random());
      // this assigns a random variable i, within the range of 0 -9, to each postion to the array
      // in turn, this creates an array of random numbers
    }
    return list;
  }
 
  public static int [] delete(int [] list, int pos){
   int [] newArray1 = new int [list.length - 1];
    // this made sure that the length of the new array was one element shorter than the original, which is neccessary because we are deleting an element
    for(int i = 0; i < 9; i++){
      // this is a for loop that goes through the array
      // when figuring out this code I kept getting 0 for the value of the array I wanted to remove and than the last element of the array would dissappear
      // in order to fix this I made the element that I wanted to remove the last element of the array
    if (i < pos){
     newArray1[i]= list[i];
      // so while the value of i was less than the position of the element, the array was just copied
    }
      else if (i >= pos){
      newArray1[i]= list[i + 1];
        // however once the postion of the array was greater than or equal to the element that was being removed, I copied everything a position down. 
        // this ensure that the rest of the array remained the same but that the element to be deleted wasn't there
      }
       }
   return newArray1;
  }
  
  public static int [] remove (int [] list, int target){
      int identical = 0;
    // one of the main problems with this is that in any given array there could be multiple elements with the same value
    for(int i = 0; i < 10; i++){
      if(list[i] == target){
        identical = identical + 1;
        // this basically goes through the array and counts the number of times the target element is in the array
         System.out.println("i: " + i);
          }
    }
    
    int [] newArray2 = new int [list.length - identical];
    // because we want the new array to be the length of the original array without the target values, it is imporant to subtract the number of times the element occurs in the array from the total length
    int progression = 0;
    for(int i = 0; i < list.length - identical; i++){
    if(list[i] != target){
    newArray2[i]= list[i + progression];
    }
      else{
        progression = progression + 1;
        newArray2[i] = list[i+progression];
      }
      // this code works perfectly for removing one element from the array, but doesn't quite work in removing multiple elements
  }
   return newArray2;
  }
  
  
  
  
}

