/* 
CSE 17
Jaina Hausser
jkh221
Homework #3
Program: Text Messaging
*/
import java.util.Scanner;
import java.io.File;
public class TextDecoder {
  
 private static TextMessage messages [] = new TextMessage [10]; 
 private static int msgCount = 0;
  
  public TextDecoder () {

  }
  
  public static void readMessagesFromFile (File msgFile) {
    Scanner input = new Scanner(messages.txt); 
    while (input.hasNextLine()) {
       String line = input.nextLine();
       recipient = line.substring(0,10);
       keyPresses = line.substring(11);
       
       messages [msgCount] = getDecodedMessage(keyPresses);
       msgCount++;
         
    }
  }
  
  public static void printMessages() {
    System.out.println(recipient + ":   " + messages[i]);
    
  }
  
  public static void main (String [] args) {
    
     if (args.length != 1) {
      System.out.println("Error: No arguments found");
      System.exit(0);
     }
    
    
    String filename = args[0];
    File msgFile = new File(messages.txt);
    
    
    readMessagesFromFile(msgFile);
    
    for (int i =0; i < messages.length; i++) {
      printMessages();
      
    }
    
  }
  
}