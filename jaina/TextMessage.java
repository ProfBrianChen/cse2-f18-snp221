/* 
CSE 17
Jaina Hausser
jkh221
Homework #3
Program: Text Messaging
*/

public class TextMessage {
  
   private static String recipient;
   private static String keyPresses;
  
  public TextMessage (String recipient, String keyPresses) {
    
  }
  
  public static String getRecipient () {
    recipient = this.recipient;
    return recipient;
  }
  
  public static String getDecodedMessage (String keyPresses) {
   
    String decodedMessage = "";
    
    for (int i = 0; i < keyPresses.length(); i++) {
      
      if (keyPresses.charAt(i) == '2' && keyPresses.charAt(i+1) == '2' && keyPresses.charAt(i+2) == '2' ) {
        decodedMessage.charAt(i) = 'C';     
      }
      else if  (keyPresses.charAt(i) == '2' && keyPresses.charAt(i+1) == '2' && keyPresses.charAt(i+2) != '2' ) {
        decodedMessage.charAt(i) = 'B';
      }
      else if  (keyPresses.charAt(i) == '2' && keyPresses.charAt(i+1) != '2' && keyPresses.charAt(i+2) != '2' ) {
        decodedMessage.charAt(i) = 'A';
      }
      else if (keyPresses.charAt(i) == '3' && keyPresses.charAt(i+1) == '3' && keyPresses.charAt(i+2) == '3' ) {
        decodedMessage.charAt(i) = 'F';     
      }
      else if  (keyPresses.charAt(i) == '3' && keyPresses.charAt(i+1) == '3' && keyPresses.charAt(i+2) != '3' ) {
        decodedMessage.charAt(i) = 'E';
      }
      else if  (keyPresses.charAt(i) == '3' && keyPresses.charAt(i+1) != '3' && keyPresses.charAt(i+2) != '3' ) {
        decodedMessage.charAt(i) = 'D';
      }
      else if (keyPresses.charAt(i) == '4' && keyPresses.charAt(i+1) == '4' && keyPresses.charAt(i+2) == '4' ) {
        decodedMessage.charAt(i) = 'I';     
      }
      else if  (keyPresses.charAt(i) == '4' && keyPresses.charAt(i+1) == '4' && keyPresses.charAt(i+2) != '4' ) {
        decodedMessage.charAt(i) = 'H';
      }
      else if  (keyPresses.charAt(i) == '4' && keyPresses.charAt(i+1) != '4' && keyPresses.charAt(i+2) != '4' ) {
        decodedMessage.charAt(i) = 'G';
      }
      else if (keyPresses.charAt(i) == '5' && keyPresses.charAt(i+1) == '5' && keyPresses.charAt(i+2) == '5' ) {
        decodedMessage.charAt(i) = 'L';     
      }
      else if  (keyPresses.charAt(i) == '5' && keyPresses.charAt(i+1) == '5' && keyPresses.charAt(i+2) != '5' ) {
        decodedMessage.charAt(i) = 'K';
      }
      else if  (keyPresses.charAt(i) == '5' && keyPresses.charAt(i+1) != '5' && keyPresses.charAt(i+2) != '5' ) {
        decodedMessage.charAt(i) = 'J';
      }
      else if (keyPresses.charAt(i) == '6' && keyPresses.charAt(i+1) == '6' && keyPresses.charAt(i+2) == '6' ) {
        decodedMessage.charAt(i) = 'O';     
      }
      else if  (keyPresses.charAt(i) == '6' && keyPresses.charAt(i+1) == '6' && keyPresses.charAt(i+2) != '6' ) {
        decodedMessage.charAt(i) = 'N';
      }
      else if  (keyPresses.charAt(i) == '6' && keyPresses.charAt(i+1) != '6' && keyPresses.charAt(i+2) != '6') {
        decodedMessage.charAt(i) = 'M';
      }
      else if (keyPresses.charAt(i) == '7' && keyPresses.charAt(i+1) == '7' && keyPresses.charAt(i+2) == '7' && keyPresses.charAt(i+3) == '7' ) {
        decodedMessage.charAt(i) = 'S';     
      }
      else if (keyPresses.charAt(i) == '7' && keyPresses.charAt(i+1) == '7' && keyPresses.charAt(i+2) == '7' && keyPresses.charAt(i+3) != '7' ) {
        decodedMessage.charAt(i) = 'R';     
      }
      else if (keyPresses.charAt(i) == '7' && keyPresses.charAt(i+1) == '7' && keyPresses.charAt(i+2) != '7' && keyPresses.charAt(i+3) != '7' ) {
        decodedMessage.charAt(i) = 'Q';     
      }
      else if (keyPresses.charAt(i) == '7' && keyPresses.charAt(i+1) != '7' && keyPresses.charAt(i+2) != '7' && keyPresses.charAt(i+3) != '7' ) {
        decodedMessage.charAt(i) = 'P';     
      }
      else if (keyPresses.charAt(i) == '8' && keyPresses.charAt(i+1) == '8' && keyPresses.charAt(i+2) == '8' ) {
        decodedMessage.charAt(i) = 'V';     
      }
      else if  (keyPresses.charAt(i) == '8' && keyPresses.charAt(i+1) == '8' && keyPresses.charAt(i+2) != '8') {
        decodedMessage.charAt(i) = 'U';
      }
      else if  (keyPresses.charAt(i) == '8' && keyPresses.charAt(i+1) != '8' && keyPresses.charAt(i+2) != '8' ) {
        decodedMessage.charAt(i) = 'T';
      }
      else if (keyPresses.charAt(i) == '9' && keyPresses.charAt(i+1) == '9' && keyPresses.charAt(i+2) == '9' && keyPresses.charAt(i+3) == '9' ) {
        decodedMessage.charAt(i) = 'Z';     
      }
      else if (keyPresses.charAt(i) == '9' && keyPresses.charAt(i+1) == '9' && keyPresses.charAt(i+2) == '9' && keyPresses.charAt(i+3) != '9' ) {
        decodedMessage.charAt(i) = 'Y';     
      }
      else if (keyPresses.charAt(i) == '9' && keyPresses.charAt(i+1) == '9' && keyPresses.charAt(i+2) != '9' && keyPresses.charAt(i+3) != '9' ) {
        decodedMessage.charAt(i) = 'X';     
      }
      else if (keyPresses.charAt(i) == '9' && keyPresses.charAt(i+1) != '9' && keyPresses.charAt(i+2) != '9' && keyPresses.charAt(i+3) != '9' ) {
        decodedMessage.charAt(i) = 'W';     
      }
      else if (keyPresses.charAt(i) == '1') {
        decodedMessage.charAt(i) = '.';   
      }
       else if (keyPresses.charAt(i) == '0') {
        decodedMessage.charAt(i) = ' ';   
       }
      
       return decodedMessage;
    }
    
   
  }
  
}
