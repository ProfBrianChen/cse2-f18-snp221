//   Samantha Pastoressa
// September 9th, 2018, CSE 02 -311
//Document your program. What does MPG do? Place you comments here!
//Assignment: My bicycle cyclometer (meant to measure speed, distance, etc.) records two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel during that time. For two trips, given time and rotation count, your program should
//print the number of minutes for each trip
//print the number of counts for each trip
//print the distance of each trip in miles
//print the distance for the two trips combined

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      // our input data. Document your variables by placing your
		// comments after the 
      // State the purpose of each variable.

	   	int secsTrip1=480;  
      // declares the variable SecsTrips1 as an integer while also assigning it a value
   // by declaring the variable, you are giving it a name and a type
      //by assigning a number to the variable, you are giviing it a value
      int secsTrip2=3220;  
      // declares the variable SecsTrips2 as an integer while also assigning it a value
		int countsTrip1=1561;  
      // declares the variable countsTrip1 as an integer while also assigning it a value
		int countsTrip2=9037; 
      // declares the variable countsTrips2 as an integer while also assigning it a value
      
// our intermediate variables and output data. Document!
double wheelDiameter=27.0;  
      // declares the variable wheelDiamter a double while assigning it the value 27.0
      //By using the declaration of double, this ensures that the decimal places will be included in the value
  	double PI=3.14159; 
      //// declares the variable PI a double while assigning it the value 3.14159
  	int feetPerMile=5280; 
      // declares the variable feetPerMile an integer while assigning it the value 5280
  	int inchesPerFoot= 12;  
      //declares the variable inchesPerFoot an integer while assigning it the value 12
  	int secondsPerMinute=60;  
      //declares the variable secondsPerMinute an integer while assigning it the value 60
      double secondsPerMinuteDouble =(int)secondsPerMinute;
      // because the directions wanted 8.0 minutes and 53.66666 seconds, and both the secondsPerMinute variable and the SecsTrips2 and SecsTrips1 variables were declared as integers,
      //it was necessary to convert at least one of the varibales (per function) to a double
      //integers implicity cast to doubles
	double distanceTrip1, distanceTrip2,totalDistance; 
      //this decalres distanceTrip1, distanceTrip2 and totalDistance as variables without assinging them a value, the value assigningment occurs later in the code
      
      
      
System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinuteDouble)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinuteDouble)+" minutes and had "+
       	      countsTrip2+" counts.");
      
      
     distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; 
      // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
	
//Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
 

      
      
      


	}  //end of main method   
} //end of class

