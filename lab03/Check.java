// Lab 3
// Samantha Pastoressa
// September 13, 2018
// CSE002 - 311
// The user has gone out to dinner with friends. After they receive the bill, they decide to split the check evenly. 
//Write a program that uses the Scanner class to obtain from the user the original cost of the check, the percentage tip they wish to pay, 
//and the number of ways the check will be split. Then determine how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner;
// Document your program. Place your comments here!
// This imports Scanner, which is needed to be able to use it
//
public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
         Scanner myScanner = new Scanner( System.in );
          //declares an instance of the Scanner object and calls the Scanner constructor
      System.out.print("Enter the original cost of the check in the form xx.xx: ");
          // asks the user to input a value for the original cost of the check
   double checkCost = myScanner.nextDouble();
          // this declares the variable checkCost a double, and than assigns the user's input as its value
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) :" );
          // this allows the user to input the percentage tip that they would like to leave
double tipPercent = myScanner.nextDouble();
          // similar to before, this accepts the value for the tip as a double and assigns it to the declared variable, tipPercent
tipPercent /= 100;
 //We want to convert the percentage into a decimal value
  System.out.print("Enter the number of people who went out to dinner:");
          // this allows the user to input the number of people at the dinner
          //which will enable them to to determine how much each will pay when they split the check evenly
int numPeople = myScanner.nextInt();
          // this accepts the value for the number of people as an integer and assigns it to the decleared variable, numPeople
double totalCost;
          // this declares the variable "totalCost" as a double
double costPerPerson;
          // this declares the variable "costPerPerson" as a double
int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
totalCost = checkCost * (1 + tipPercent);
          // this calculates the total cost of the check including the tip
costPerPerson = totalCost / numPeople;
// this calculates the cost for each individual person
dollars=(int)costPerPerson;
      //get the whole amount, dropping decimal fraction
dimes=(int)(costPerPerson * 10) % 10;
          //get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
          // this gives an integer value for the dimes
pennies=(int)(costPerPerson * 100) % 10;
          // this gives an integer value for the pennies
      //dimes and pennis is important because the amount of dollars was calculated as integer, but we want to a value with 2 decimal places
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
          // this returns to the userm the total amount each person in the group owes
                   
               

             

}  //end of main method   
  	} //end of class

