// Lab 4
// Samantha Pastoressa
// September 20, 2018
// CSE002 - 311
// write a program that will pick a random card from the deck so you can practice your tricks alone. 
//Use a random number generator to select a number from 1 to 52 (inclusive).  Each number represents one card, and the suits are grouped: 
//Cards 1-13 represent the diamonds, 14-26 represent the clubs, then hearts, then spades.  
//In all suits, card identities ascend in step with the card number: 14 is the ace of clubs, 15 is the 2 of clubs, and 26 is the king of clubs.

public class CardGenerator {
  public static void main (String[] args){
    
   int randomNumber = (int)(Math.random()*52 + 1);
  
     //System.out.println(randomNumber);
    /// I used the above code to test if my output was correct
  String suit = "";
    String facecard = "";
    // creates an empty string for the suit and facecard 
    
       
   if (randomNumber <= 13){
   facecard = "Diamonds";
   }
    else if(randomNumber <= 26 & randomNumber > 13 ){
   facecard= "Clubs";
    }  
    else if(randomNumber <=39 & randomNumber > 26 ){
  facecard = "Hearts";
  }
    else if(randomNumber <= 52 && randomNumber > 39){
    facecard = "Spades";
  }
 
    // this assigns either diamonds, clubs, hearts or spades to the card depending on what number in the deck it was
  
     
    
   int card = randomNumber % 13;
    // this finds the remainder of the random number and 13
    //System.out.println(card);
   // I used the above code to test if my output was correct
    switch(card){
     case 11:
       suit  = "Jack"; 
     break;
      case 12:
       suit = "Queen"; 
        //card = suit;
     break;  
        case 0:
       suit = "King"; 
     break;  
        case 14:
       suit = "Ace"; 
     break; 
       case 1:
       suit = "Ace"; 
     break; 
    }
    
    // This assigns remainders of 11, 12, 14, 1 and 0 to a face card. The 0 is nessassary bc the 13 % 13 is 0, but we want it to say king 
    // Additionally, there are no ones in a card deck, thus, the one should be an ace 
    
    
    if (suit == ""){
System.out.println("You picked the " + card + " of " + facecard);
      // if there is no suit, then this will return the integer value of the card and the face of the card
}
  else {

 System.out.println("You picked the " + suit + " of " + facecard);
    // if there is a suit then this will return the suit and the face of the card
  }
 
  
    
  }
}