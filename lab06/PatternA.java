import java.util.Scanner;
  public class PatternA{
      public static void main (String [] args){
       Scanner myScanner = new Scanner( System.in );
        
        boolean pyramidLength;
        int length = 0;
          
        while(true){
            System.out.print("How long would you like your pyramid to be (between 1 and 10)?  ");
            pyramidLength = myScanner.hasNextInt();
             if (pyramidLength){
                  length = myScanner.nextInt();
                break;
                }
                else{
                 System.out.println("Error - input of integer type needed: ");
                    myScanner.next();
     }    
    }
        
        
         
          for (int i = 1; i <= length; i++) {
            for (int j = 1; j <= i; j++){
                System.out.print(j+" ");
            }
            System.out.println();
       
        }

    }
}
